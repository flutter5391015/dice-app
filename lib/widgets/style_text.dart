import 'package:flutter/material.dart';

class StyleText extends StatelessWidget {
  final String title;

  const StyleText({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(
        color: Colors.white,
        fontSize: 28,
      ),
    );
  }
}
