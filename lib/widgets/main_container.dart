import 'package:dice_app/widgets/dice_roller.dart';
import 'package:flutter/material.dart';

const startAlignment = Alignment.topLeft;
const endAlignment = Alignment.bottomRight;

class MainContainer extends StatelessWidget {
  final List<Color> colors;

  const MainContainer({super.key, required this.colors});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: startAlignment,
            end: endAlignment,
            colors: colors,
          ),
        ),
        child: const DiceRoller());
  }
}
