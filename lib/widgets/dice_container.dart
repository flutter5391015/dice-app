import 'package:flutter/material.dart';

class DiceContainer extends StatelessWidget {
  final List<Widget> children;
  const DiceContainer({super.key, required this.children});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: children,
      ),
    );
  }
}
