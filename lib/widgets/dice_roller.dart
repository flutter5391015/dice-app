import 'package:dice_app/widgets/dice_container.dart';
import 'package:dice_app/widgets/style_text.dart';
import 'package:flutter/material.dart';
import 'dart:math';

final random = Random();

class DiceRoller extends StatefulWidget {
  const DiceRoller({super.key});

  @override
  State<DiceRoller> createState() => _DiceRollerState();
}

class _DiceRollerState extends State<DiceRoller> {
  var activeDiceImage = 'assets/images/dice-1.png';

  void rollDice() {
    var randomNumber = random.nextInt(6) + 1;
    setState(() {
      activeDiceImage = 'assets/images/dice-$randomNumber.png';
    });
  }

  @override
  Widget build(BuildContext context) {
    return DiceContainer(
      children: [
        const StyleText(title: "Hello Word"),
        Image.asset(
          activeDiceImage,
          width: 200,
        ),
        TextButton(
          onPressed: rollDice,
          style: TextButton.styleFrom(
            padding: const EdgeInsets.all(12),
            backgroundColor: Colors.white,
            textStyle: const TextStyle(fontSize: 14, color: Colors.indigo),
          ),
          child: const Text('Roll the Dice'),
        )
      ],
    );
  }
}
