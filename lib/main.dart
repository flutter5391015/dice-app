import 'package:dice_app/widgets/main_container.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    home: Scaffold(
      body: MainContainer(colors: const [Colors.deepPurple, Colors.indigo]),
    ),
  ));
}
